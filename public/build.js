if (window.location.pathname === '/' && window.location.hash.startsWith('#user_')) {
	var userId = window.location.hash.match(/\d+/gi) || [];
	if (userId.length > 0) {
		var date = new Date();
		date.setDate(date.getDate() + 7); 
		setCookie('userId', userId[0], date.getTime());
	}

}

Question = function(config) {
	this.model = config;
	this._container = {};
	this._element = {};
	this._input = {};
	this._buttons = [];

};

Question.prototype.init = function() {
	if (this.model.id.startsWith('rithmomera') && QuestApp.data.rithmomera == undefined) {
			QuestApp.data.rithmomera = this.model.id;
			QuestApp.question['before_rithmomera'].init();
			return;
	}
	this._container = document.createElement("div");
	var panel = document.createElement("div");
	var button = document.createElement("div");
	var classList = this.model.classList.split(" ");

	for(var i in classList) {
		if(classList[i] === "question-radio-random") {
			classList[i] =classList[i] + "-" + getRandomInt(1, 3);
		}
		this._container.classList.add(classList[i]);
	}
	this._container.appendChild(this._initText());
	this._container.appendChild(panel);
	this._container.appendChild(button);
	if(this.model.typeQuestion === "input") {
		this._initTypeInput(panel);
		this._initTypeButton(button);
		this._initErrorMessage(button);
	}else if(this.model.typeQuestion === "color") {
		this._initTypeColor(panel);
	}else if(this.model.typeQuestion === "inputControl") {
		this._initTypeInputControl(panel);
	}else if(this.model.typeQuestion === "numerolog") {
		this._initTypeInputTriple(panel);
		this._initTypeButton(button);
	}else if(this.model.typeQuestion === "bigText") {
		QuestApp.data.rithmomera = this.model.id;
		QuestApp.question['before_rithmomera'].init();
		this._initTypeBigText(panel);
		this._initTypeButton(button);
	}else if(this.model.typeQuestion === "form") {
		this._initTypeForm(panel);
		this._initErrorMessage(button);
	}else if(this.model.typeQuestion === "counter") {
		this._initTypeCounter(panel);
	}else{
		this._initTypeButton(button);
	}

	this._initProgressBar(button);
	QuestApp.initSlide(this._container);
	setTimeout(function() {
		$('body').trigger('questionChanged');
	}, 1000);
	if (this.model.yandexGoal !== undefined && this.model.yandexGoal !== "") {
		yaCounter47572876.reachGoal(this.model.yandexGoal);
	}
};

Question.prototype._initErrorMessage = function(element) {
	var $msg = $('<div />').addClass('question-error-msg');
	$msg.html('Заполните пустые поля или проверьте введенные данные.<br>Возможно, что-то не так.');
	$msg.appendTo($(element));
	return element;
};

Question.prototype._initTypeButton = function(element) {
	for(item in this.model.buttons) {
		var button = this._initButton(this.model.buttons[item]);
		element.appendChild(button);
		this._buttons.push(button);
	}
	element.classList.add("question-submit");
	if(this.model.typeQuestion === "input"){
		element.classList.add("disable");
	}
	document.addEventListener("keyup", this._initKeyupEnter);
	return this._buttons;
};

Question.prototype._initTypeInput = function(element) {
	var input = this.model.input;
	if(input.type === "text") {
		this._input = this._initInputText(input);
		element.appendChild(this._input);
	} else if(input.type === "date") {
		this._input = this._initInputDate(input);
		element.appendChild(this._input);
	} else if(input.type === "select") {
		this._input = this._initInputSelect(input);
		element.appendChild(this._input);
	} else if(input.type === "radio") {
		this._input = this._initInputRadio(input);
		element.appendChild(this._input);
	}
	element.classList.add("question-input");
};

Question.prototype._initText = function() {
	var element = document.createElement("div");
	element.id = "text_"+this.model.storageID;
	element.innerHTML = this._initTextVariables(this.model.text);
	element.classList.add("question-text");
	return element;
};

Question.prototype._initButton = function(item) {
	var self = this;
	var element = document.createElement("button");
	element.id = this.model.id+"_"+item.id;
	element.innerHTML = item.text;
	if(item.text === "" || item.text === null) {
		element.innerHTML = icon.play;
	}
	element.classList.add("btn");
	element.addEventListener("click", function() {
		if (item.yandexGoal !== undefined && item.yandexGoal !== "") {
			yaCounter47572876.reachGoal(item.yandexGoal);
		}
		if (item.target !== null && item.target.startsWith('http')) {
			window.location.href = item.target;
			return;
		}
		if(!(self.model.typeQuestion === "input" && (self._input.value === undefined || self._input.value.length === 0))) {
			if ($(self._input).data('yandexGoal') !== undefined && $(self._input).data('yandexGoal') !== "") {
				yaCounter47572876.reachGoal($(self._input).data('yandexGoal'));
			}
			if(self.model.action === "add") {
				QuestApp.addData(self._input.dataset.id, parseInt(self._input.value, 10));
				QuestApp.setData(self.model.id, self._input.value);
			} else if(self._input != undefined && self._input.dataset != undefined && self._input.dataset.id != undefined && self._input.dataset.id != "") {
				QuestApp.setData(self._input.dataset.id, self._input.value);
				if (self._input.dataset.id == 'intro_2' && self._input.value == 0) {
					var blockInit = true;
					QuestApp.question[self._initTextVariables('kit')].init();
				} else {
					var blockInit = false;
				}	
			}
			if (!blockInit) {
				if(item.value != undefined) {
					QuestApp.setData(self.model.storageID, item.value);
				}
				console.log(QuestApp.data);
				if(item.target != undefined) {
					QuestApp.question[self._initTextVariables(item.target)].init();
				}else{
					QuestApp.question[self._initTextVariables(self._initTextVariables(self.model.target))].init();
				}				
			}
			document.removeEventListener("keyup", self._initKeyupEnter);
			if(self._container.classList.contains("question-start") && isMobile.any()) {
				QuestApp.audio.play();
			}
			if(self._container.classList.contains("question-finish")) {
				window.location.reload();
			}
		}
	});
	return element;
};

Question.prototype._initInputText = function(item) {
	var element = document.createElement("input");
	element.id = this.model.id+"_"+this.model.storageID;
	element.dataset.id = this.model.storageID;
	element.type = "text";
	element.addEventListener("keyup", function() {
		if(this.value != "") {
			$(".question-input").addClass("active");
			$(".question-submit").removeClass("disable");
			$('.question-error-msg').slideUp();
		}else{
			$(".question-input").removeClass("active");
			$(".question-submit").addClass("disable");
		};
	});
	return element;
};

Question.prototype._initInputDate = function(item) {
	var self = document.createElement("div");
	var element = document.createElement("div");
	var el = {
		day:{placeholder:"дд", type:"text", max:2, element:{}, next:"month"},
		month:{placeholder:"мм", type:"text", max:2, element:{}, next:"year"},
		year:{placeholder:"гггг", type:"text", max:4, element:{}, next:""},
		date:{placeholder:"", type:"hidden", element:{}}
	};
	var button = document.createElement("button");
	button.classList.add("calendar");
	for(var i in el) {
		var item = document.createElement("input");
		item.type = el[i].type;
		item.placeholder = el[i].placeholder;
		item.dataset.id = i;
		item.dataset.next = el[i].next;
		item.classList.add("date-input-"+i);
		el[i].element = item;
		element.appendChild(item);
		if(i != "date") {
			item.addEventListener("input", function() {
				var value = "";
				var elem = el[this.dataset.id];
				for(var j=0; j<this.value.length; j++) {
					if(!isNaN(this.value[j])) {
						if(j < elem.max) {
							value += this.value[j];
							if(elem.max- 1 === j && (this.dataset.next != undefined && this.dataset.next != "")) {
								$(el[this.dataset.next].element).focus();
							}
						}else{
							if(this.dataset.next != undefined && this.dataset.next != "") {
								el[this.dataset.next].element.value += this.value[j];
								el[this.dataset.next].element.dispatchEvent(new Event("input"));
							}else{
								value = value.slice(1);
								value += this.value[j];
							}
						}
					}
				}
				this.value = value;
				el.date.element.value = el.year.element.value+"-"+el.month.element.value+"-"+el.day.element.value;
				element.value = el.date.element.value;
				if((el.year.element.value != "" && el.month.element.value != "" && el.day.element.value) && (parseInt(el.day.element.value, 10) <= 31 && parseInt(el.month.element.value, 10) <= 12) && (parseInt(el.day.element.value, 10) > 0 && parseInt(el.month.element.value, 10) > 0)) {
					$(".question-submit").removeClass("disable");
					$('.question-error-msg').slideUp();
				}else{
					$(".question-submit").addClass("disable");
					element.value = "";
				}
			});
		}else{
			$(item).datepicker({
				changeMonth: true,
				changeYear: true,
				dateFormat: "yy-mm-dd",
				maxDate: "+1m +1w",
				yearRange: "1900:2017",
				defaultDate: "1987-01-01",
				onSelect:function() {
					var arr = el.date.element.value.split("-");
					el.year.element.value = arr[0];
					el.month.element.value = arr[1];
					el.day.element.value = arr[2];
					element.value = el.date.element.value;
					if(el.year.element.value != "" && el.month.element.value != "" && el.day.element.value) {
						$(".question-submit").removeClass("disable");
						$('.question-error-msg').slideUp();
					}else{
						$(".question-submit").addClass("disable");
					}
				}
		    });
			button.addEventListener("click", function() {
				$(item).datepicker("show");
			});
		}
	};
	$('body').addClass('datepicker--btm');
	element.classList.add("date-input");
	element.appendChild(button);
	element.id = this.model.id+"_"+this.model.storageID;
	element.dataset.id = this.model.storageID;
	return element;
};

Question.prototype._initInputRadio = function(item) {
	var element = document.createElement("div");
	var number = 1;
	element.id = this.model.id+"_"+this.model.storageID;
	element.dataset.id = this.model.storageID;
	for(i in item.variant) {
		var label = document.createElement("label");
		var radio = document.createElement("input");
		radio.id = this.model.storageID+"_"+number;
		radio.type = "radio";
		radio.name = this.model.storageID;
		radio.value = item.variant[i].value;
		if (item.variant[i].yandexGoal !== undefined && item.variant[i].yandexGoal !== "") {
			radio.dataset.yandexGoal = item.variant[i].yandexGoal;
		}		
		radio.classList.add("radio");
		radio.addEventListener("click", function() {
			element.value = this.value;
			if (this.dataset.yandexGoal !== undefined && this.dataset.yandexGoal !== "") {
				element.dataset.yandexGoal = this.dataset.yandexGoal;
			}			
			$(".question-submit").removeClass("disable");
		});
		label.htmlFor = radio.id;
		label.innerHTML = item.variant[i].name;
		element.appendChild(radio);
		element.appendChild(label);
		number++;
	}
	return element;
};

Question.prototype._initInputSelect = function(item) {
	var element = document.createElement("select");
	element.id = this.model.id+"_"+this.model.storageID;
	element.dataset.id = this.model.storageID;
	for(i in item.variant) {
		var option = document.createElement("option");
		option.innerHTML = item.variant[i].name;
		option.addEventListener("click", function() {
			element.value = this.value;
		});
		element.appendChild(option);
	}
	return element;
};

Question.prototype._initTextVariables = function(text) {
	var items = new Array();
	var right = text;
	var pos = right.indexOf("{{");
	while(pos > -1) {
		left = right.substring(0, pos);
		items.push(left);
		right = right.substring(pos + 2);
		pos = right.indexOf("}}");
		if (pos == -1) {
			throw Error("Template not valid");
		}
		left = right.substring(0, pos);
		items.push(this._getValueById(left));
		right = right.substring(pos + 2);
		pos = right.indexOf("{{");
		if (pos == -1) {
			items.push(right);
		}
	}
	if (items.length === 0) {
		return text;
	} else {
		return items.join("");
	}

};

Question.prototype._getValueById = function(variable) {
	var id = variable.split(":");
	var templateValues = config.templateValues;
	if(this.model.templateValues != undefined && this.model.templateValues[id[0]] != undefined){
		templateValues = this.model.templateValues;
	}
	if(id.length > 1 && templateValues[id[0]] != undefined) {
		if(id[1] === "" || id[1] === undefined) {
			id[1] = id[0];
		}
		if(templateValues[id[0]][QuestApp.data[id[1]]] === undefined) {
			return QuestApp.data[id[1]];
		}
		return templateValues[id[0]][QuestApp.data[id[1]]];
	}else{
		return QuestApp.data[id[0]];
	}
};

Question.prototype._initTypeColor = function(element) {
	var self = this;
	var input = [];
	var panelInput = document.createElement("div");
	var panelButton = document.createElement("div");
	for(i in this.model.input) {
		var item = this.model.input[i];
		var button = document.createElement("button");
		var text = document.createElement("div");
		button.id = this.model.id+"_"+item.id;
		button.dataset.color = item.value;
		button.classList.add("color");
		button.classList.add("color-"+item.value);
		button.addEventListener("click", function() {
			self.color = this.dataset.color;
			panelButton.classList.remove("disable");
			$(".color").removeClass("active");
			$(this).addClass("active");
		});
		/* text.innerHTML = item.name;*/
		text.classList.add("color-text");
		button.appendChild(text);
		panelInput.appendChild(button);
		input.push(button);
	}
	for(item in this.model.buttons) {
		var button = document.createElement("button")
		button.innerHTML = icon.play;
		button.classList.add("btn");
		button.addEventListener("click", function() {
			if(self.color != undefined && !$.isEmptyObject(self.color)) {
				$(".background").addClass("color-background-"+self.color);
				QuestApp.setData("color", self.color);
				console.log(QuestApp.data);
				if(item.target != undefined) {
					QuestApp.question[self._initTextVariables(item.target)].init(self._element);
				}else{
					QuestApp.question[self._initTextVariables(self.model.target)].init(self._element);
				}
			}
		});
		panelButton.appendChild(button);
		this._buttons.push(button);
	}
	this._input = input;
	panelInput.classList.add("question-input");
	panelButton.classList.add("question-submit");
	panelButton.classList.add("disable");
	element.appendChild(panelInput);
	element.appendChild(panelButton);
	return element;
};

Question.prototype._initTypeInputControl = function(element) {
	var self = this;
	var input = this.model.input;
	var data = QuestApp.data;
	var variant = input.variant;
	var max = Math.max(data.opros_s, data.opros_z, data.opros_d, data.opros_L);
	var panelInput = document.createElement("div");
	var panelButton = document.createElement("div");
	element.appendChild(panelInput);
	element.appendChild(panelButton);
	input.variant = [];
	if(data.opros_L === max) {
		input.variant.push(variant[0]);
		data.opros = "lubov_1";
	}
	if(data.opros_d === max) {
		input.variant.push(variant[1]);
		data.opros = "dengi_1";
	}
	if(data.opros_z === max) {
		input.variant.push(variant[2]);
		data.opros = "zdorov_1";
	}
	if(data.opros_s === max) {
		input.variant.push(variant[3]);
		data.opros = "selfknowledge_1";
	}
	if(input.variant.length > 1) {
		if(input.variant.length === 2) {
			this._container.classList.add("question-duo");
		}else if (input.variant.length === 3) {
			this._container.classList.add("question-radio-random-1");
		}else if (input.variant.length === 4) {
			this._container.classList.add("question-quadro");
		}
		this._initTypeInput(panelInput);
		this._initTypeButton(panelButton);
	}else{
		setTimeout(function() {
			QuestApp.question[self._initTextVariables(self.model.target)].init();
		},100);
	}
};

Question.prototype._initTypeInputTriple = function(element) {
	var items = {
		num:{
			icon:icon.num,
			element:this._initTypeInputNumerolog(document.createElement("div"))
		},
		zod:{
			icon:icon.zod,
			element:this._initTypeInputZodiak(document.createElement("div"))
		},
		rithm:{
			icon:icon.rythm,
			element:this._initTypeInputRitmolog(document.createElement("div"))
		},
	};
	var panel = document.createElement("div");
	for(var i in items) {
		items[i].element.classList.add("text-numerolog","hide");
		items[i].element.id = i;
		element.appendChild(items[i].element);
		items[i].button = document.createElement("button");
		items[i].button.innerHTML = items[i].icon;
		items[i].button.classList.add("btn-triple");
		items[i].button.dataset.id = i;
		items[i].button.addEventListener("click", function() {
			$(".btn-triple").removeClass("active");
			$(this).addClass("active");
			$(".text-numerolog").addClass("hide");
			$("#"+this.dataset.id).removeClass("hide");
		});
		panel.appendChild(items[i].button);
	}
	element.appendChild(panel);
	items.num.button.classList.add("active");
	items.num.element.classList.remove("hide");
	return element;
};

Question.prototype._initTypeInputNumerolog = function(element) {
	var tmp = this._initTextVariables('{{dateBirth}}').split("-");
	var sum = 0;
	var num = 0;
	for(var i in tmp) {
		var arr = tmp[i].split("");
		for(var j in arr) {
			sum += parseInt(arr[j], 10);
		}
	}
	while(sum > 9){
		var string = sum.toString();
		var arr = string.split("");
		sum = 0;
		for(var j in arr) {
			sum += parseInt(arr[j], 10);
		}
	}
	QuestApp.data.numerolog = sum;
	QuestApp.data.textNumerolog = sum;
	element.appendChild(this._initText());
	return element;
};

Question.prototype._initTypeInputRitmolog = function(element) {
	var tmp = this._initTextVariables('{{dateBirth}}').split("-");
	var sum = 0;
	var num = 0;
	for(var i in tmp) {
		var arr = tmp[i].split("");
		for(var j in arr) {
			sum += parseInt(arr[j], 10);
		}
	}
	while(sum > 9){
		var string = sum.toString();
		var arr = string.split("");
		sum = 0;
		for(var j in arr) {
			sum += parseInt(arr[j], 10);
		}
	}
	QuestApp.data.ritmolog = "q"+sum;
	QuestApp.data.textNumerolog = "q"+sum;
	element.appendChild(this._initText());
	return element;
};

Question.prototype._initTypeInputZodiak = function(element) {
	var date = this._initTextVariables('{{dateBirth}}').split("-")[2];
	var month = this._initTextVariables('{{dateBirth}}').split("-")[1];
	var result = "";
	if (month == 1 && date >= 20 || month == 2 && date <= 18) result = "Водолей";
	else if (month == 2 && date >= 19 || month == 3 && date <= 20) result = "Рыбы";
	else if (month == 3 && date >= 21 || month == 4 && date <= 19) result = "Овен";
	else if (month == 4 && date >= 20 || month == 5 && date <= 20) result = "Телец";
	else if (month == 5 && date >= 21 || month == 6 && date <= 21) result = "Близнецы";
	else if (month == 6 && date >= 22 || month == 7 && date <= 22) result = "Рак";
	else if (month == 7 && date >= 23 || month == 8 && date <= 22) result = "Лев";
	else if (month == 8 && date >= 23 || month == 9 && date <= 22) result = "Дева";
	else if (month == 9 && date >= 23 || month == 10 && date <= 22) result = "Весы";
	else if (month == 10 && date >= 23 || month == 11 && date <= 21) result = "Скорпион";
	else if (month == 11 && date >= 22 || month == 12 && date <= 21) result = "Стрелец";
	else if (month == 12 && date >= 22 || month == 1 && date <= 19) result = "Козерог";
	QuestApp.data.zodiak = result;
	QuestApp.data.textNumerolog = result;
	element.appendChild(this._initText());
	return element;
};

Question.prototype._initTypeBigText = function(element) {
	var self = this;
	var button = document.createElement("button");
	var text1 = $(self._container.childNodes[0])[0].innerHTML.substring(43, 900);
	var text2 = $(self._container.childNodes[0])[0].innerHTML.substring(900, $(self._container.childNodes[0])[0].innerHTML.length);
	var scroll = true;
	$(self._container.childNodes[0])[0].innerHTML = '<h2>Итак, результаты</h2><div class="text"><span class="big-text">'+text1+'</span><span class="big-text" style="display:none">'+text2+'</span></div>';
	button.classList.add("down");
	button.addEventListener("click", function() {
		self._openBigText();
		scroll = false;
	});
	document.onwheel = function() {
		if(scroll) {
			self._openBigText();
		}
		scroll = false;
	};
	document.ontouchmove = function() {
		if(scroll) {
			self._openBigText();
		}
		scroll = false;
	};
	element.classList.add("question-input-bigtext");
	element.appendChild(button);
	$(this._container.childNodes[0]).addClass("question-text-bigtext");
};

Question.prototype._openBigText = function() {
	var self = this;
	$(self._container.childNodes[0]).addClass("question-text-bigtext-2");
	$(".big-text").slideDown(1500);
	setTimeout(function() {
		$(".big-text").css("display","inline");
		$.fn.fullpage.destroy('all');
	}, 1500);
	$(".question-submit button").css('margin-bottom', '75px');
	$(".question-submit button").on('click', function() {
		setTimeout(function(){
			document.getElementById("app").innerHTML = "";
			QuestApp.initFullpage(document.getElementById("app"));
		}, 100);
	});
};

Question.prototype._initTypeForm = function(element) {
	var self = this;
	var top = document.createElement("div");
	var bottom = document.createElement("div");
	var panelButton = document.createElement("div");
	var contact = {
		type:document.createElement("div"),
		input:document.createElement("input")
	};
	var datetime = {
		text:document.createElement("div"),
		date:document.createElement("input"),
		time:document.createElement("input"),
	};
	var type = [{text:"",placeholder:"Введите свой номер телефона:", value:'phone'},{text:"",placeholder:"Ваш электронный адрес:", value:'email'},{text:"",placeholder:"Логин Skype:", value:'skype'}];

	contact.type.classList.add("type");
	contact.input.type = "text";
	$(contact.input).inputmask('+79999999999');
	contact.input.addEventListener("input", function() {
		if($(datetime.date).inputmask('isComplete') && $(datetime.time).inputmask('isComplete') && $(contact.input).inputmask('isComplete')) {
			panelButton.classList.remove("disable");
		}
	});
	for(var i in type) {
		var label = document.createElement("label");
		var radio = document.createElement("input");
		if (i == 0) {
			radio.checked = true;
		}
		radio.id = "form_type_"+i;
		radio.type = "radio";
		radio.name = "form_type";
		radio.classList.add("radio");
		radio.value = type[i].value;
		radio.dataset.placeholder = type[i].placeholder;
		radio.addEventListener("click", function() {
			contact.input.placeholder = this.dataset.placeholder;
			switch (this.value) {
				case 'email':
					$(contact.input).inputmask('email');
					break;
				case 'phone':
					$(contact.input).inputmask('+79999999999');
					break;
				default:
					$(contact.input).inputmask('*{5,31}');
			}
		});
		label.htmlFor = radio.id;
		label.innerHTML = type[i].text;
		contact.type.appendChild(radio);
		contact.type.appendChild(label);
	}

	datetime.date.type = "text";
	datetime.time.type = "text";
	datetime.date.placeholder = "дд-мм";
	datetime.time.placeholder = "чч:мм";
	$(datetime.date).inputmask({ alias: "datetime", inputFormat: "dd-mm", placeholder: 'дд-мм' });
	$(datetime.time).inputmask({ alias: "datetime", inputFormat: "HH:MM", placeholder: 'чч:мм' });
	datetime.text.innerHTML = "Выберите удобное время и день:";
	datetime.text.classList.add("calendar");
	datetime.time.addEventListener("input", function() {
		var count = 0;
		var value = "";
		var before = "";
		var after = "";
		for(var j=0; j<this.value.length; j++) {
			if(!isNaN(this.value[j])) {
				if(count < 4) {
					value += this.value[j];
					count++;
				}else{
					value = value.slice(1);
					value += this.value[j];
				}
			}
		}
		if(value.length >= 4) {
			before = value.substring(0,2);
			after = value.substring(2, value.length);
			value = before+":"+after;
			this.value = value;
		}
		if($(datetime.date).inputmask('isComplete') && $(datetime.time).inputmask('isComplete') && $(contact.input).inputmask('isComplete')) {
			panelButton.classList.remove("disable");
		}
	});
	datetime.date.addEventListener("input", function() {
		var count = 0;
		var value = "";
		var before = "";
		var after = "";
		for(var j=0; j<this.value.length; j++) {
			if(!isNaN(this.value[j])) {
				if(count < 4) {
					value += this.value[j];
					count++;
				}else{
					value = value.slice(1);
					value += this.value[j];
				}
			}
		}
		if(value.length >= 4) {
			before = value.substring(0,2);
			after = value.substring(2, value.length);
			value = before+"-"+after;
			this.value = value;
		}
		if($(datetime.date).inputmask('isComplete') && $(datetime.time).inputmask('isComplete') && $(contact.input).inputmask('isComplete')) {
			panelButton.classList.remove("disable");
		}
	});
	top.classList.add("question-input");
	top.appendChild(contact.type);
	top.appendChild(contact.input);
	bottom.classList.add("question-input");

	$('body').addClass('datepicker--btm');
	divvedDate = document.createElement('div');
	divvedDate.classList.add('question-input__date');
	divvedDate.appendChild(datetime.date);

	divvedTIme = document.createElement('div');
	divvedTIme.classList.add('question-input__time');
	divvedTIme.appendChild(datetime.time);

	bottom.appendChild(datetime.text);
	bottom.appendChild(divvedDate);
	bottom.appendChild(divvedTIme);

	var modelBtns = this.model.buttons;

	for(item in modelBtns) {
		var button = document.createElement("button");
		var itemBtn = modelBtns[item];
		button.innerHTML = icon.play;
		button.classList.add("btn");
		button.addEventListener("click", function() {
			var radio = document.querySelector('.type input[type="radio"][name="form_type"]:checked') || null;
			if($(datetime.date).inputmask('isComplete') && $(datetime.time).inputmask('isComplete') && $(contact.input).inputmask('isComplete') && radio) {
				var year = (new Date()).getFullYear();
				var timeString = datetime.time.value + ':00';
				var dateTimes = datetime.date.value.split('-');
				var dateTime = year + '-' + dateTimes[1] + '-' + dateTimes[0] + ' ' + timeString;

				QuestApp.data.contactWay = radio.value;
				QuestApp.data.contactAddress = contact.input.value;
				QuestApp.data.contactDatetime = dateTime;

				if (itemBtn.yandexGoal !== undefined && itemBtn.yandexGoal !== "") {
					yaCounter47572876.reachGoal(itemBtn.yandexGoal);
				}
				console.log(QuestApp.data)
				QuestApp.question[self._initTextVariables(self.model.target)].init();
			}
		});
		panelButton.appendChild(button);
		this._buttons.push(button);
	}
	panelButton.classList.add("question-submit");
	panelButton.classList.add("disable");

	element.appendChild(top);
	element.appendChild(bottom);
	element.appendChild(panelButton);
	$(".fp-tableCell")[0].appendChild(QuestApp.initPolicy());
	$(datetime.date).datepicker({
		dateFormat: "dd-mm",
		ignoreReadonly: true,
   		allowInputToggle: true
	});
	$("#ui-datepicker-div").addClass("date");
	$('body').addClass('datepicker--btm');
};

Question.prototype._initTypeCounter = function(element) {
	var self = this;
	var userId = getCookie('userId');
	var deadline;
	if (userId !== '') {
		$.ajax({
			method: 'POST',
			url: '/customer/result',
			data: {
				userId: userId
			},
			success: function(data) {
				if (data.deadline !== undefined) {
					self._initCountDown(data.deadline);
				} else if (data.text !== undefined)	{
					setTimeout(function() {
						const result = config.questions.find( question => question.id === 'result' );
						result.text = '<h2>Результаты</h2><div class="text">' + data.text + '</div>';
						QuestApp.question[result.id].init();

					}, 1000);
				} else {
					setTimeout(function() {
						$('.question-text').empty();
						$('.question-text').text('Произошла ошибка. Перезагрузите страницу.');
					}, 1000);					
				}
			},
			error: function() {
				setTimeout(function() {
						$('.question-text').empty();
						$('.question-text').text('Произошла ошибка. Перезагрузите страницу.');
					}, 1000);
			}
		});	
	} else {
		$.ajax({
			method: 'POST',
			url: '/customer/add',
			data: {
				name: QuestApp.data.name,
				gender: QuestApp.data.gender,
				dateBirth: QuestApp.data.dateBirth,
				textNumerolog: QuestApp.data.numerolog || null,
				textRitmolog: QuestApp.data.ritmolog || null,
				textZodiak: QuestApp.data.zodiak || null,
				color: QuestApp.data.color,
				intro: QuestApp.data.intro_2,
				contactWay: QuestApp.data.contactWay,
				contactAddress: QuestApp.data.contactAddress,
				contactDatetime: QuestApp.data.contactDatetime,
				comment: QuestApp.data.comment || null,
			},
			success: function(data) {
				if (data.id !== undefined) {
					var date = new Date();
					date.setDate(date.getDate() + 7); 
					setCookie('userId', data.id, date.getTime());
					self._initCountDown(data.deadline);
				}		
			}
		});		
	}	
};

Question.prototype._initCountDown = function(deadline) {
	var faded = false;
    var x = setInterval(function() {
      var now = new Date().getTime();
	  var t = deadline - now;
      var seconds = Math.floor( (t/1000) % 60 );
	  var minutes = Math.floor( (t/1000/60) % 60 );
	  var hours = Math.floor( (t/(1000*60*60)) % 24 );
	  var days = Math.floor( t/(1000*60*60*24) );

	  if (faded) {
	  	$("#question--countdown").html('<span>' + ('0' + (hours + days * 24)).slice(-2) + '</span> : <span>' + ('0' + minutes).slice(-2) + '</span> : <span>' + ('0' + seconds).slice(-2) + '</span>');
	  } else {
	  	$("#question--countdown").css('visibility', 'visible');
	  	$("#question--countdown").hide().html('<span>' + ('0' + (hours + days * 24)).slice(-2) + '</span> : <span>' + ('0' + minutes).slice(-2) + '</span> : <span>' + ('0' + seconds).slice(-2) + '</span>').fadeIn();
	  	faded = true;
	  }
	  
      
	
      if (t < 0) {
        clearInterval(x);
        $("#question--countdown").html("Время вышло");
      }
    }, 1000);
};

Question.prototype._initProgressBar = function(element) {
	if(this.model.id.indexOf("opros_") != -1) {
		var bar = document.createElement("div");
		var progressPoint = document.createElement('span');
		var active = Math.round((100/27) * (this.model.id.split("opros_")[1] / 100) * 12);

		for (var i=0;i<12;i++) {
			var clone = progressPoint.cloneNode();

			if (i < active) {
				clone.classList.add('active');
			}

			bar.appendChild(clone);
		}
		
		bar.classList.add("progress-bar");
		element.appendChild(bar);
	}
};

QuestApp = (function() {
	this.url = {
		main:"/",
		audio:"/main_ambiance.mp3"
	};
	this.audio = {};
	this.question = {};
	this.data = {};
	this.fullpage = false;
	this.block = document.createElement("div");
	function setQuestion(name, question) {
		this.question[name] = question;
	};
	function setData(name, value) {
		this.data[name] = value;
	};
	function addData(name, value) {
		if(this.data[name] != undefined) {
			this.data[name] += value;
		}else{
			this.data[name] = 0;
			this.data[name] += value;
		}
	};
	function start(element) {
		initFullpage(element);
		for(item in config.questions){
			setQuestion(config.questions[item].id, new Question(config.questions[item]));
		}
	};
	function initFullpage(element) {
		var section = document.createElement("section");
		section.classList.add("section");
		section.classList.add("slide");
		section.appendChild(_initLogo());
		section.appendChild(_initMute());
		section.appendChild(this.block);
		element.appendChild(section);
		if(!this.fullpage) {
			$(element).fullpage({
				navigation: false,
				responsiveWidth: 900,
			});
			this.fullpage = true;
		}
	};
	function initSlide(element) {
		$(this.block).fadeOut(850, function() {
	  		this.innerHTML = "";
	  		this.appendChild(element);
			$(this).fadeIn(1150);
		});
	};
	function initPolicy() {
		var a = document.createElement("a");
		a.classList.add("policy");
		a.innerHTML = "Политика конфеденциальности";
		a.href = "/";
		return a;
	};
	function initAudio() {
		this.audio = document.createElement("audio");
		this.audio.classList.add("audio");
		this.audio.loop = true;
		this.audio.autoplay = true;
		this.audio.src = url.audio;
		return this.audio;
	};
	function _initMute() {
		var button = document.createElement("button");
		button.classList.add("sound");
		button.classList.add("unmute");
		button.addEventListener("click", function() {
			if(this.classList.contains("unmute")) {
				this.classList.remove("unmute");
				this.classList.add("mute");
				QuestApp.audio.pause();
			} else {
				this.classList.remove("mute");
				this.classList.add("unmute");
				QuestApp.audio.play();
			}
		});
		return button;
	};
	function _initLogo() {
		var div = document.createElement("div");
		div.classList.add("logo");
		div.addEventListener("click", function(){
			window.location.reload();
		});
		return div;
	};
	return {
		start: start,
		data: this.data,
		question: this.question,
		block: this.block,
		url: this.url,
		element: this.element,
		setData: setData,
		addData: addData,
		setQuestion: setQuestion,
		initFullpage: initFullpage,
		initSlide: initSlide,
		initPolicy: initPolicy,
		initAudio: initAudio,
		audio:audio
	};
})();

function getRandomInt(min, max) {
	return Math.floor(Math.random() * (max - min + 1)) + min;
}

(function( factory ) {
    if ( typeof define === "function" && define.amd ) {
        // AMD. Register as an anonymous module.
        define( [ "../widgets/datepicker" ], factory );
    } else {
        // Browser globals
        factory( jQuery.datepicker );
    }
}( function( datepicker ) {
    datepicker.regional.ru = {
        closeText: "Закрыть",
        prevText: "&#x3C;",
        nextText: "&#x3E;",
        currentText: "Сегодня",
        monthNames: [ "Январь","Февраль","Март","Апрель","Май","Июнь",
        "Июль","Август","Сентябрь","Октябрь","Ноябрь","Декабрь" ],
        monthNamesShort: [ "Янв","Фев","Мар","Апр","Май","Июн",
        "Июл","Авг","Сен","Окт","Ноя","Дек" ],
        dayNames: [ "воскресенье","понедельник","вторник","среда","четверг","пятница","суббота" ],
        dayNamesShort: [ "вск","пнд","втр","срд","чтв","птн","сбт" ],
        dayNamesMin: [ "Вс","Пн","Вт","Ср","Чт","Пт","Сб" ],
        weekHeader: "Нед",
        dateFormat: "dd.mm.yy",
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ""
    };
    datepicker.setDefaults( datepicker.regional.ru );
    return datepicker.regional.ru;
}));

var icon = {
	play:'<span class="arrow--play">\
<svg xmlns="http://www.w3.org/2000/svg" id="Layer_1" data-name="Слой 1">\
 <style>.cls-2 {fill: none;stroke: #fff;stroke-miterlimit: 10;stroke-width: 0.75px;}</style>\
  <g class="cls-1">\
    <g>\
      <circle class="cls-2" cx="42.56" cy="23.68" r="2.17"/>\
      <g>\
        <circle class="cls-2" cx="2.54" cy="23.68" r="2.17"/>\
        <circle class="cls-2" cx="12.51" cy="23.68" r="2.17"/>\
        <circle class="cls-2" cx="22.47" cy="23.68" r="2.17"/>\
        <circle class="cls-2" cx="32.44" cy="23.68" r="2.17"/>\
        <circle class="cls-2" cx="52.37" cy="23.68" r="2.17"/>\
        <g>\
          <circle class="cls-2" cx="41.19" cy="2.54" r="2.17"/>\
          <circle class="cls-2" cx="48.24" cy="9.59" r="2.17"/>\
          <circle class="cls-2" cx="55.29" cy="16.63" r="2.17"/>\
          <circle class="cls-2" cx="62.33" cy="23.68" r="2.17"/>\
        </g>\
        <g>\
          <circle class="cls-2" cx="41.19" cy="44.82" r="2.17"/>\
          <circle class="cls-2" cx="48.24" cy="37.77" r="2.17"/>\
          <circle class="cls-2" cx="55.29" cy="30.73" r="2.17"/>\
          <circle class="cls-2" cx="62.33" cy="23.68" r="2.17"/>\
        </g>\
      </g>\
    </g>\
  </g>\
</svg>\
</span>',
	num:'<svg version="1.0" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"\
			 width="92px" height="70px" viewBox="0 0 92 70" enable-background="new 0 0 92 70" xml:space="preserve">\
		<g>\
			<g>\
				<path fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" d="M66.043,12.946c-0.276-0.478-0.888-0.642-1.366-0.366L48.5,21.92\
					V13.5c0-0.552-0.448-1-1-1s-1,0.448-1,1v9.574L27.438,34.08c-0.223,0.129-0.37,0.334-0.444,0.562\
					c-0.142,0.083-0.272,0.188-0.36,0.34c-0.275,0.469-0.112,1.068,0.364,1.339L46.5,47.417V56.5c0,0.552,0.448,1,1,1s1-0.448,1-1\
					v-7.945l15.529,8.836c0.476,0.271,1.084,0.11,1.358-0.359c0.275-0.469,0.112-1.068-0.364-1.339L48.5,46.292V24.229l17.177-9.917\
					C66.155,14.036,66.319,13.424,66.043,12.946z M46.5,45.154l-17.246-9.813L46.5,25.384V45.154z"/>\
			</g>\
		</g>\
		</svg>',
	zod:'<svg version="1.0" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="92px" height="70px" viewBox="0 0 92 70" enable-background="new 0 0 92 70" xml:space="preserve">\
		<g>\
			<path fill="#FFFFFF" d="M46,31.5c1.93,0,3.5,1.57,3.5,3.5s-1.57,3.5-3.5,3.5c-1.93,0-3.5-1.57-3.5-3.5S44.07,31.5,46,31.5 M46,23.5\
				c-6.351,0-11.5,5.149-11.5,11.5S39.649,46.5,46,46.5c6.351,0,11.5-5.149,11.5-11.5S52.351,23.5,46,23.5L46,23.5z"/>\
		</g>\
		<g>\
			<path fill="#FFFFFF" d="M46,14.5c11.304,0,20.5,9.196,20.5,20.5S57.304,55.5,46,55.5S25.5,46.304,25.5,35S34.696,14.5,46,14.5\
				 M46,12.5c-12.426,0-22.5,10.074-22.5,22.5c0,12.426,10.074,22.5,22.5,22.5c12.426,0,22.5-10.074,22.5-22.5\
				C68.5,22.574,58.426,12.5,46,12.5L46,12.5z"/>\
		</g>\
		<g>\
			<g>\
				<path fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" d="M45.5,31.5h1v-17h-1V31.5z M45.5,55.5h1v-17h-1V55.5z\
					 M43.219,32.817l-14.722-8.5l-0.5,0.866l14.722,8.5L43.219,32.817z M48.781,37.183l14.722,8.5l0.5-0.866l-14.722-8.5\
					L48.781,37.183z M35.317,17.497l8.5,14.722l0.866-0.5l-8.5-14.722L35.317,17.497z M47.317,38.281l8.5,14.722l0.866-0.5\
					l-8.5-14.722L47.317,38.281z M49.5,34.5v1h17v-1H49.5z M42.5,34.5h-17v1h17V34.5z M56.683,17.497l-0.866-0.5l-8.5,14.722\
					l0.866,0.5L56.683,17.497z M35.317,52.503l0.866,0.5l8.5-14.722l-0.866-0.5L35.317,52.503z M49.281,33.683l14.722-8.5l-0.5-0.866\
					l-14.722,8.5L49.281,33.683z M42.719,36.317l-14.722,8.5l0.5,0.866l14.722-8.5L42.719,36.317z"/>\
			</g>\
		</g>\
		</svg>',
	rythm:'<svg version="1.0" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"\
			 width="92px" height="70px" viewBox="0 0 92 70" enable-background="new 0 0 92 70" xml:space="preserve">\
		<g>\
			<path fill="#FFFFFF" d="M51.297,14.5l-4.685,13.337L45.678,30.5H48.5h11.718L42.387,50.329L47.37,37.21l1.029-2.71H45.5H31.643\
				l7.711-20H51.297 M51.734,12.5H39.143c-0.703,0-1.333,0.435-1.582,1.092L29.5,34.5c-0.197,0.519-0.047,1.275,0.269,1.733\
				c0.173,0.25,0.508,0.306,0.867,0.306c0.298,0,0.612-0.039,0.864-0.039h14l-7.677,20.213c-0.099,0.262,0.003,0.557,0.243,0.703\
				c0.094,0.057,0.198,0.085,0.302,0.085c0.161,0,0.32-0.066,0.434-0.193l23.264-25.871c0.446-0.497,0.559-1.209,0.287-1.819\
				c-0.272-0.61-1.185-1.117-1.853-1.117c-1.933,0-12,0-12,0l4.816-13.711c0.197-0.519,0.125-1.102-0.19-1.559\
				C52.81,12.773,52.29,12.5,51.734,12.5L51.734,12.5z"/>\
		</g>\
		</svg>'
};

document.addEventListener('DOMContentLoaded', function() {
	var userId = getCookie('userId');

	QuestApp.start(document.getElementById("app"));
	$.datepicker.setDefaults( $.datepicker.regional[ "ru" ] );
	document.body.appendChild(QuestApp.initAudio());

	if (userId !== '') {
		QuestApp.question["countdown"].init();	
	} else {		
		QuestApp.question["intro_1"].init();				
	}

});

document.addEventListener("keyup", function(event) {
	switch(event.keyCode) {
	case 13:
		if($(".btn").html() != undefined) {
			$(".btn")[0].dispatchEvent(new Event("click"));
		}
		break;
	default:
		break;
	}
});

$('body').on('questionChanged', function() {
	$('.question-input-text input:not([class^="question-input__"] input)').focus();
});
$('body').on('click', '.question-result-comment .question-submit button, #result_b1', function() {
	$.ajax({
			method: 'POST',
			url: '/customer/update',
			data: {
				userId: getCookie('userId'),
				comment: QuestApp.data.result_comment || null,
			}
		});		
});
$('body').on('click', '.question-submit button', function() {
  var full = true;
  $('input').each(function() {
    if ($(this).val() == "") {
    	full = false;
    }
  });

  if (!full) {
  	$('.question-error-msg').slideDown();
  }
})
var isMobile = {
	Android: function() {
	 return navigator.userAgent.match(/Android/i);
	},
	BlackBerry: function() {
	 return navigator.userAgent.match(/BlackBerry/i);
	},
	iOS: function() {
	 return navigator.userAgent.match(/iPhone|iPad|iPod/i);
	},
	Opera: function() {
	 return navigator.userAgent.match(/Opera Mini/i);
	},
	Windows: function() {
	 return navigator.userAgent.match(/IEMobile/i);
	},
	SMART_TV: function() {
	 return navigator.userAgent.match(/SMART-TV/i);
	},
	any: function() {
	 return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows() || isMobile.SMART_TV());
	}
};


function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

String.prototype.startsWith = function(str){
 if(this.indexOf(str)===0){
  return true;
 }else{
   return  false;
 }
};