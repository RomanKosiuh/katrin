<?php

namespace App\EventListener;

use Doctrine\ORM\Event\PreUpdateEventArgs;
use App\Entity\Customer;

class UserNotification
{
	protected $twig;
    protected $mailer;

    public function __construct(\Twig_Environment $twig, \Swift_Mailer $mailer)
    {
        $this->twig = $twig;
        $this->mailer = $mailer;
    }

    public function preUpdate(PreUpdateEventArgs $args)
    {
        $entity = $args->getEntity();

        if (!$entity instanceof Customer) {
            return;
        }

        if (empty($entity->getAdminText()) || !$args->hasChangedField('adminText') || $entity->getNotified()) {
        	return;
        }

        switch ($entity->getContactWay()) {
        	case 'email':
        		$message = (new \Swift_Message('Вы получили ответ от консультанта'))
			        ->setFrom(getenv('ROBOT_EMAIL'))
			        ->setTo($entity->getContactAddress())
			        ->setBody(
			            $this->twig->render(
			                'emails/notification.html.twig',
                            ['userId' => $entity->getId()]
			            ),
			            'text/html'
			        )
			    ;

			    $this->mailer->send($message);
			    $entity->setNotified(true);

        		break;
        	case 'phone':
                require_once(__DIR__."/../../private/smsprofi/transport.php");
                $api = new \Transport();

                $params = array(
                    'text' => 'Ваш результат готов. Проверить - https://katrin.tech/#user_'.$entity->getId(),
                    'source' => 'Katrin.tech'
                );

                $phones = array($entity->getContactAddress());

                $send = $api->send($params,$phones);
                $entity->setNotified(true);
        		break;

        	case 'skype':
        		break;
        	
        	default:
        		break;
        }

	    return;
    }
}