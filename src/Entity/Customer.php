<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CustomerRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Customer
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $gender;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $dateBirth;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $textNumerolog;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $textRitmolog;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $textZodiak;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $color;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $intro;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $contactWay;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $contactAddress;

    /**
     * @ORM\Column(type="datetime")
     */
    private $contactDatetime;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $submitDatetime;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $adminText;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $comment;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $status = 'new';

    /**
     * @ORM\Column(type="boolean")
     */
    private $notified = false;

    public function __toString()
    {
        return (string)$this->name;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getGender(): ?string
    {
        return $this->gender;
    }

    public function setGender(string $gender): self
    {
        $this->gender = $gender;

        return $this;
    }

    public function getDateBirth(): ?\DateTimeInterface
    {
        return $this->dateBirth;
    }

    public function setDateBirth(?\DateTimeInterface $dateBirth): self
    {
        $this->dateBirth = $dateBirth;

        return $this;
    }

    public function getTextNumerolog(): ?string
    {
        return $this->textNumerolog;
    }

    public function setTextNumerolog(?string $textNumerolog): self
    {
        $this->textNumerolog = $textNumerolog;

        return $this;
    }

    public function getTextRitmolog(): ?string
    {
        return $this->textRitmolog;
    }

    public function setTextRitmolog(?string $textRitmolog): self
    {
        $this->textRitmolog = $textRitmolog;

        return $this;
    }

    public function getTextZodiak(): ?string
    {
        return $this->textZodiak;
    }

    public function setTextZodiak(?string $textZodiak): self
    {
        $this->textZodiak = $textZodiak;

        return $this;
    }

    public function getColor(): ?string
    {
        return $this->color;
    }

    public function setColor(?string $color): self
    {
        $this->color = $color;

        return $this;
    }

    public function getIntro(): ?string
    {
        return $this->intro;
    }

    public function setIntro(string $intro): self
    {
        $this->intro = $intro;

        return $this;
    }

    public function getContactWay(): ?string
    {
        return $this->contactWay;
    }

    public function setContactWay(string $contactWay): self
    {
        $this->contactWay = $contactWay;

        return $this;
    }

    public function getContactAddress(): ?string
    {
        return $this->contactAddress;
    }

    public function setContactAddress(string $contactAddress): self
    {
        $this->contactAddress = $contactAddress;

        return $this;
    }

    public function getContactDatetime(): ?\DateTimeInterface
    {
        return $this->contactDatetime;
    }

    public function setContactDatetime(\DateTimeInterface $contactDatetime): self
    {
        $this->contactDatetime = $contactDatetime;

        return $this;
    }

    public function getSubmitDatetime(): ?\DateTimeInterface
    {
        return $this->submitDatetime;
    }

    public function setSubmitDatetime(?\DateTimeInterface $submitDatetime): self
    {
        $this->submitDatetime = $submitDatetime;

        return $this;
    }

    public function getAdminText(): ?string
    {
        return $this->adminText;
    }

    public function setAdminText(?string $adminText): self
    {
        $this->adminText = $adminText;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment($comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getNotified()
    {
        return $this->notified;
    }

    public function setNotified($notified): self
    {
        $this->notified = $notified;

        return $this;
    }
}
