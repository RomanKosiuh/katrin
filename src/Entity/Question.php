<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\QuestionRepository")
 */
class Question
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $pk;

    /**
     * @ORM\Column(type="text")
     */
    private $text;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $typeQuestion;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $target;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $classList;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $storageID;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $action;

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $buttons;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $inputType;

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $input;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $templateValuesType;

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $templateValues;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $yandexGoal;

    public function __toString()
    {
        return (string)$this->pk;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getPk(): ?string
    {
        return $this->pk;
    }

    public function setPk(string $pk): self
    {
        $this->pk = $pk;

        return $this;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(string $text): self
    {
        $this->text = $text;

        return $this;
    }

    public function getTypeQuestion(): ?string
    {
        return $this->typeQuestion;
    }

    public function setTypeQuestion(string $typeQuestion): self
    {
        $this->typeQuestion = $typeQuestion;

        return $this;
    }

    public function getTarget(): ?string
    {
        return $this->target;
    }

    public function setTarget(string $target): self
    {
        $this->target = $target;

        return $this;
    }

    public function getClassList(): ?string
    {
        return $this->classList;
    }

    public function setClassList(string $classList): self
    {
        $this->classList = $classList;

        return $this;
    }

    public function getStorageID(): ?string
    {
        return $this->storageID;
    }

    public function setStorageID(string $storageID): self
    {
        $this->storageID = $storageID;

        return $this;
    }

    public function getAction(): ?string
    {
        return $this->action;
    }

    public function setAction(string $action): self
    {
        $this->action = $action;

        return $this;
    }

    public function getButtons(): ?array
    {
        return $this->buttons;
    }

    public function setButtons(array $buttons): self
    {
        $this->buttons = $buttons;

        return $this;
    }

    public function getInputType(): ?string
    {
        return $this->inputType;
    }

    public function setInputType(string $inputType): self
    {
        $this->inputType = $inputType;

        return $this;
    }

    public function getInput(): ?array
    {
        return $this->input;
    }

    public function setInput(array $input): self
    {
        $this->input = $input;

        return $this;
    }

    public function getTemplateValuesType(): ?string
    {
        return $this->templateValuesType;
    }

    public function setTemplateValuesType(string $templateValuesType): self
    {
        $this->templateValuesType = $templateValuesType;

        return $this;
    }

    public function getTemplateValues(): ?array
    {
        return $this->templateValues;
    }

    public function setTemplateValues(array $templateValues): self
    {
        $this->templateValues = $templateValues;

        return $this;
    }

    public function getYandexGoal(): ?string
    {
        return $this->yandexGoal;
    }

    public function setYandexGoal(string $yandexGoal): self
    {
        $this->yandexGoal = $yandexGoal;

        return $this;
    }
}
