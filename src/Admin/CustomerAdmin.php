<?php

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\CoreBundle\Form\Type\DatePickerType;
use Sonata\CoreBundle\Form\Type\DateTimePickerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use App\Entity\Question;

class CustomerAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->with('Основное')
                ->add('name', TextType::class, [
                    'label' => 'Имя'
                ])
                ->add('status', ChoiceType::class, [
                    'label' => 'Статус',
                    'empty_data' => 'new',
                    'choices' => [
                        'Первичное обращение' => 'new',
                        'Повторное обращение' => 'returning'
                    ]
                ])
                ->add('gender', ChoiceType::class, [
                    'label' => 'Пол',
                    'choices' => [
                        'мужчина' => 'man',
                        'женщина' => 'woman'
                    ]
                ])
                ->add('dateBirth', DatePickerType::class, [
                    'label' => 'Дата рождения',
                    'required' => false
                ])
                ->add('textNumerolog', TextType::class, [
                    'label' => 'Текст нумеролог',
                    'required' => false
                ])
                ->add('textRitmolog', TextType::class, [
                    'label' => 'Текст ритмолог',
                    'required' => false
                ])
                ->add('textZodiak', TextType::class, [
                    'label' => 'Текст зодиак',
                    'required' => false
                ])
                ->add('color', ChoiceType::class, [
                    'label' => 'Цвет',
                    'choices' => array_flip([
                        'Черный', 'Красный', 'Оранжевый', 'Желтый', 'Зеленый', 'Голубой', 'Синий', 'Фиолетовый', 'Белый'
                    ])
                ])
                ->add('intro', ChoiceType::class, [
                    'label' => 'ВЫ ДОВОЛЬНЫ СВОЕЙ ЖИЗНЬЮ?',
                    'choices' => array_flip(['Нет', 'Да'])
                ])
                ->add('comment', TextType::class, [
                    'label' => 'Комментарий',
                    'required' => false
                ])
                ->add('contactWay', ChoiceType::class, [
                    'label' => 'Способ связи',
                    'choices' => [
                        'телефон' => 'phone',
                        'почта' => 'email',
                        'skype' => 'skype'
                    ]
                ])
                ->add('contactAddress', TextType::class, [
                    'label' => 'Адрес для связи'
                ])
                ->add('contactDatetime', DateTimePickerType::class, [
                    'label' => 'Время для связи'
                ])
                ->add('submitDatetime', DateTimePickerType::class, [
                    'label' => 'Время заявки',
                ])
            ->end()
            ->with('Ответ пользователю')
                ->add('adminText', TextareaType::class, array(
                    'label' => 'Текст ответа',
                    'required' => false
                ))
                ->add('notified', CheckboxType::class, array(
                    'label' => 'Пользовател уведомлен',
                    'required' => false
                ))
            ->end();
    }

    protected $datagridValues = [
        '_sort_order' => 'DESC',
    ];

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('name', null, array('label' => 'Имя'));
        $datagridMapper->add('status', null, array('label' => 'Статус'));
        $datagridMapper->add('gender', null, array('label' => 'Пол'));
        $datagridMapper->add('color', null, array('label' => 'Цвет'));
        $datagridMapper->add('submitDatetime', null, array('label' => 'Время заявки'));
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('name', null, array('label' => 'Имя'));
        $listMapper->addIdentifier('status', 'choice', [
            'label' => 'Статус',
            'choices' => [
                'new' => 'Первичное обращение',
                'returning' => 'Повторное обращение'
            ]
        ]);
        $listMapper->addIdentifier('gender', 'choice', array(
            'label' => 'Пол',
            'choices' => array_flip([
                'мужчина' => 'man',
                'женщина' => 'woman'
            ])
        ));
        $listMapper->addIdentifier('color', 'choice', array(
            'label' => 'Цвет',
            'choices' => [
                'Черный', 'Красный', 'Оранжевый', 'Желтый', 'Зеленый', 'Голубой', 'Синий', 'Фиолетовый', 'Белый'
            ]
        ));
        $listMapper->addIdentifier('contactDatetime', null, array(
            'label' => 'Время для связи',
            'format' => 'd.m.y H:i:s'
        ));
        $listMapper->addIdentifier('submitDatetime', null, array(
            'label' => 'Время заявки',
            'format' => 'd.m.y H:i:s'
        ));
    }
}