<?php

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use App\Form\Type\QuestionButtonsType;
use App\Form\Type\QuestionInputsType;
use App\Form\Type\QuestionTemplateValuesType;

class QuestionAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->with('Основное')
                ->add('pk', TextType::class, [
                    'label' => 'ID'
                ])
                ->add('text', TextareaType::class, [
                    'label' => 'Текст'
                ])
                ->add('typeQuestion', ChoiceType::class, [
                    'label' => 'Тип',
                    'empty_data' => '',
                    'choices' => [
                        'Кнопка' => 'button',
                        'Инпут' => 'input',
                        'Цвет'  => 'color',
                        'Нумероголия' => 'numerolog',
                        'Большой текст' => 'bigText',
                        'Форма' => 'form',
                        'Контрольный вопрос' => 'inputControl',
                        'Счетчик' => 'counter'
                    ]
                ])
                ->add('target', TextType::class, [
                    'label' => 'Цель',
                    'help' => 'ID следующего вопроса',
                    'required' => false,
                    'empty_data' => '',
                ])
                ->add('classList', TextType::class, [
                    'label' => 'Классы',
                    'help' => 'Впишите необходимые классы через пробел',
                    'required' => false,
                    'empty_data' => ''
                ])
                ->add('storageID', TextType::class, [
                    'required' => false,
                    'label' => 'Категория',
                    'empty_data' => ''
                ])
                ->add('action', TextType::class, [
                    'required' => false,
                    'label' => 'Действие',
                    'empty_data' => ''
                ])
                ->add('yandexGoal', TextType::class, [
                    'required' => false,
                    'label' => 'Идентификатор Яндекс метки',
                    'empty_data' => ''
                ])
            ->end()
            ->with('Кнопки')
                ->add('buttons', CollectionType::class, array(
                    'entry_type' => QuestionButtonsType::class,
                    'allow_add' => true,
                    'required' => false,
                    'label' => 'Кнопки',
                    'delete_empty' => true,
                    'empty_data' => []
                ))
            ->end()
            ->with('Инпут')
                ->add('inputType', ChoiceType::class, array(
                    'label' => 'Тип инпута',
                    'choices' => [
                        'Радио' => 'radio',
                        'Текст' => 'text',
                        'Дата' => 'date',
                    ],
                    'empty_data' => '',
                    'required' => false
                ))
                ->add('input', CollectionType::class, array(
                    'entry_type' => QuestionInputsType::class,
                    'allow_add' => true,
                    'required' => false,
                    'empty_data' => [],
                    'label' => 'Варианты'
                ))
            ->end()
            ->with('Переменные шаблона')
                ->add('templateValuesType', TextType::class, array(
                    'label' => 'Название переменной',
                    'required' => false,
                    'empty_data' => ''
                ))
                ->add('templateValues', CollectionType::class, array(
                    'entry_type' => QuestionTemplateValuesType::class,
                    'allow_add' => true,
                    'required' => false,
                    'empty_data' => '',
                    'label' => 'Значения'
                ))
            ->end();
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('pk', null, array('label' => 'ID'));
        $datagridMapper->add('text', null, array('label' => 'Текст вопроса'));
        $datagridMapper->add('typeQuestion', null, array('label' => 'Тип'));
        $datagridMapper->add('target', null, array('label' => 'Цель'));
        $datagridMapper->add('action', null, array('label' => 'Действие'));
        $datagridMapper->add('storageID', null, array('label' => 'Категория'));
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('pk', null, array('label' => 'ID'));
        $listMapper->addIdentifier('text', 'string',  array('template' => 'admin/list_string.html.twig'));
        $listMapper->addIdentifier('typeQuestion', null, array('label' => 'Тип'));
        $listMapper->addIdentifier('target', null, array('label' => 'Цель'));
        $listMapper->addIdentifier('storageID', null, array('label' => 'Категория'));
    }
}