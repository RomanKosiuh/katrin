<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Question;
use App\Entity\Customer;
use DateInterval;

class DefaultController extends Controller
{
    protected $mailer;

    public function __construct(\Swift_Mailer $mailer) {
        $this->mailer = $mailer;
    }
    /**
     * @Route("/", name="index")
     */
    public function index()
    {
    	$repo = $this->getDoctrine()->getRepository(Question::class);
    	$config = $repo->getQuestionsJSON();

        return $this->render('default/index.html.twig', [
        	'config' => $config
        ]);
    }
    /**
     * @Route("/customer/add", name="customer.add")
     * @Method("POST")
     */
    public function addCustomer(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $date = new \DateTime("now");

        $customer = new Customer();

        $customer->setName($request->request->get('name'));
        $customer->setGender($request->request->get('gender'));
        $customer->setDateBirth(new \DateTime($request->request->get('dateBirth')));
        $customer->setTextNumerolog($request->request->get('textNumerolog'));
        $customer->setTextRitmolog($request->request->get('textRitmolog'));
        $customer->setTextZodiak($request->request->get('textZodiak'));
        $customer->setColor($request->request->get('color'));
        $customer->setIntro($request->request->get('intro'));
        $customer->setContactWay($request->request->get('contactWay'));
        $customer->setContactAddress($request->request->get('contactAddress'));
        $customer->setContactDatetime(new \DateTime($request->request->get('contactDatetime')));
        $customer->setSubmitDatetime($date);
        $customer->setComment($request->request->get('comment'));
        $customer->setStatus('new');

        $entityManager->persist($customer);
        $entityManager->flush();

        $deadline = $customer->getSubmitDatetime()->add(new DateInterval('PT12H'));

        $this->emailNotification();

        return new JsonResponse([
            'id' => $customer->getId(),
            'deadline' => $deadline->getTimestamp() * 1000
        ]);
    }
    /**
     * @Route("/customer/update", name="customer.update")
     * @Method("POST")
     */
    public function updateCustomer(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $repo = $this->getDoctrine()->getRepository(Customer::class);
        $customer = $repo->find($request->request->get('userId'));

        if ($request->request->get('comment')) {
            $customer->setComment($request->request->get('comment'));
        } else {
            $customer->setStatus('returning');
            $this->emailNotification();
        }
        

        $entityManager->flush();       

        return new JsonResponse([
            'id' => $customer->getId()
        ]);

    }
    /**
     * @Route("/customer/result", name="customer.result")
     * @Method("POST")
     */
    public function result(Request $request)
    {
        $repo = $this->getDoctrine()->getRepository(Customer::class);;

        $customer = $repo->find($request->request->get('userId'));

        $deadline = $customer->getSubmitDatetime()->add(new DateInterval('PT12H'));

        if ($adminText = $customer->getAdminText()) {
            return new JsonResponse([
                'text' =>  $adminText
            ]);
        } else {
            return new JsonResponse([
                'deadline' =>  $deadline->getTimestamp() * 1000
            ]);
        }        
    }

    private function emailNotification() {
        $adminEmail = getenv('ADMIN_EMAIL');

        $message = (new \Swift_Message('Новая заявка на сайте katrin.tech'))
            ->setFrom(getenv('ROBOT_EMAIL'))
            ->setTo($adminEmail)
            ->setBody('Новая заявка на сайте katrin.tech')
        ;

        $this->mailer->send($message);
    }
}
