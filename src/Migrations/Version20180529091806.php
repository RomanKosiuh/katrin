<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180529091806 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE question CHANGE text text LONGTEXT DEFAULT NULL, CHANGE target target VARCHAR(255) DEFAULT NULL, CHANGE storage_id storage_id VARCHAR(255) DEFAULT NULL, CHANGE buttons buttons LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', CHANGE template_values template_values LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\'');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE question CHANGE text text LONGTEXT NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE target target VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE storage_id storage_id VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE buttons buttons LONGTEXT NOT NULL COLLATE utf8mb4_unicode_ci COMMENT \'(DC2Type:array)\', CHANGE template_values template_values LONGTEXT NOT NULL COLLATE utf8mb4_unicode_ci COMMENT \'(DC2Type:array)\'');
    }
}
