<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180529092954 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE question CHANGE text text LONGTEXT NOT NULL, CHANGE type_question type_question VARCHAR(255) DEFAULT NULL, CHANGE class_list class_list LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\'');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE question CHANGE text text LONGTEXT DEFAULT NULL COLLATE utf8mb4_unicode_ci, CHANGE type_question type_question VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE class_list class_list VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci');
    }
}
