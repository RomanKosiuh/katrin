<?php

namespace App\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\FormType;

class QuestionInputsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('id', TextType::class, [
                'label' => 'ID варианта',
                'empty_data' => ''
            ])
            ->add('name', TextType::class, [
                'label' => 'Текст варианта',
                'empty_data' => ''
            ])
            ->add('value', TextType::class, [
                'label' => 'Баллы',
                'empty_data' => ''
            ])
            ->add('yandexGoal', TextType::class, [
                'label' => 'Идентификатор Яндекс метки',
                'empty_data' => ''
            ])
        ;
    }

    public function getParent()
    {
        return FormType::class;
    }
}