<?php

namespace App\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\FormType;

class QuestionButtonsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('id', TextType::class, [
                'label' => 'ID кнопки',
                'empty_data' => ''
            ])
            ->add('text', TextType::class, [
                'label' => 'Текст кнопки',
                'empty_data' => ''
            ])
            ->add('target', TextType::class, [
                'label' => 'Цель кнопки'
            ])
             ->add('yandexGoal', TextType::class, [
                'label' => 'Идентификатор Яндекс метки',
                'empty_data' => ''
            ])
        ;
    }

    public function getParent()
    {
        return FormType::class;
    }
}