<?php

namespace App\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\FormType;

class QuestionTemplateValuesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('id', TextType::class, [
                'label' => 'ID переменной',
                'empty_data' => ''
            ])
            ->add('value', TextareaType::class, [
                'label' => 'Значение переменной',
                'empty_data' => ''
            ])
        ;
    }

    public function getParent()
    {
        return FormType::class;
    }
}