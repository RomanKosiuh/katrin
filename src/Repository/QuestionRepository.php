<?php

namespace App\Repository;

use App\Entity\Question;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Question|null find($id, $lockMode = null, $lockVersion = null)
 * @method Question|null findOneBy(array $criteria, array $orderBy = null)
 * @method Question[]    findAll()
 * @method Question[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class QuestionRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Question::class);
    }

    public function getPks()
    {
        $res =  $this->createQueryBuilder('q')
            ->select('q.pk')
            ->orderBy('q.pk', 'ASC')
            ->getQuery()
            ->getResult()
        ;

        return array_map(function($a) {
            return $a['pk'];
        }, $res);
    }
    public function getQuestionsJSON()
    {
        $res = $this->createQueryBuilder('q')
            ->select('q.pk as id', 'q.text', 'q.typeQuestion', 'q.target', 'q.classList', 'q.storageID', 'q.action', 'q.buttons', 'q.input', 'q.inputType', 'q.templateValues', 'q.templateValuesType', 'q.yandexGoal')
            ->orderBy('q.id', 'ASC')
            ->getQuery()
            ->getResult()
        ;

        $result = '{"questions":[';

        foreach ($res as $r) {
            $result .= '{';

            foreach ($r as $key => $value) {
                if (gettype($value) == 'array') {
                    if ($key == 'input' && $r['inputType'] !== null && $r['inputType'] !== "") {
                        $result .= '"'.$key.'":{"type":"'.$r['inputType'].'","variant":'.json_encode(array_values($value)).'},';

                        unset($r['inputType']);
                    } else if ($key == 'templateValues' && $r['templateValuesType'] !== null && $r['templateValuesType'] !== "") {
                        $result .= '"'.$key.'":{"'.$r['templateValuesType'].'":'.$this->getTemplateValuesJSON($value).'},';

                        unset($r['templateValuesType']);
                    } else {
                       if (count($value) > 0) {
                            $result .= '"'.$key.'":'.json_encode(array_values($value)).',';
                        }
                    }                    
                } else {
                    $result .= '"'.$key.'":"'.$value.'",';
                }
            }

            $result = substr($result, 0, -1) . "},";
        }

        $result = substr($result, 0, -1) . ']}';

        return $result;
    }

    private function getTemplateValuesJSON($templateValues) {
        $result = "{";

        foreach ($templateValues as $templateValue) {
            $template = '"$id":"$value",';
            $vars = array(
              '$id' => $templateValue['id'],
              '$value' => $templateValue['value'],
            );

            $result .= strtr($template, $vars);
       }

       return substr($result, 0, -1) . '}';
    }
}
